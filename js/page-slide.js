/**
 * Developer Dolphin Lim
 * **/

class PageSlider {
	constructor(slider, content_slider, menu, data_slider) {
		this.slider = slider; // Блок со слайдером
		this.content_slider = content_slider; // Блок с контентом
		this.menu = menu; // Блок меню
		this.data_slider = data_slider; // Данные слайдера
		
		/**
			{
				'name': 'Slide1',
				'image': 'src.image',
				'content': 'html.elements'
			}
		**/

		this.initSlider();
	}

	/** Инициализация слайдера **/
	initSlider() {
		for (var index = 0; index < this.data_slider.length; index++) {
			/** Создание слайда **/
			var slide = document.createElement('div');
			slide.className = 'slide';
			slide.style.backgroundImage = `url('${this.data_slider[index]['image']}')`;
			this.slider.appendChild(slide);

			/** Создание пункта меню **/
			var item_menu = document.createElement('a');
			item_menu.className = 'item-menu';
			item_menu.textContent = this.data_slider[index]['name'];
			item_menu.setAttribute('numberSlide', index);
			item_menu.addEventListener('click', (event) => {
				this.showSlide(event.target.getAttribute('numberSlide'));
				this.activeSlide(event.target.getAttribute('numberSlide'));
			});
	
			this.menu.appendChild(item_menu);
		}

		var currentSlide, numberSlide = 0;
		
		this.showSlide(currentSlide);
		this.activeSlide(numberSlide);
	}

	/** Показ/Отображение слайда **/
	showSlide(index_slide) {
		var slider_children = this.slider.children;
		for (var i = 0; i < slider_children.length; i++) {
			if (index_slide == i) {
				slider_children[i].style.display = 'block';
				slider_children[i].className = 'slide fadeEffect';
				this.updateContentSlide(this.data_slider[i]['content']);
			} else {
				slider_children[i].style.display = 'none';
				slider_children[i].style.display = 'slide';
			}
		}
	}

	/** Обновление контента слайда **/
	updateContentSlide(content) {
		this.content_slider.innerHTML = content;
	}

	/** Активная страница **/
	activeSlide(i) {
		var items_menu = this.menu.children;
		for (var index = 0; index < items_menu.length; index++){
			items_menu[index].className = 'item-menu';
		}
		items_menu[i].className = 'item-menu active';
	}
}