var data = [
	{
		'name': 'Главная',
		'image': 'https://cdn.allwallpaper.in/wallpapers/1920x1080/3564/sun-mountains-mountainscapes-snow-landscapes-1920x1080-wallpaper.jpg',
		'content': '<h1 style="color: #fff; font-family: Ubuntu; font-weight: 100;">Главная страница</h1>'
	},
	{
		'name': 'Услуги',
		'image': 'https://images8.alphacoders.com/574/thumb-1920-574374.jpg',
		'content': '<h1 style="color: #fff; font-family: Ubuntu; font-weight: 100;">Что мы предлагаем?</h1>'
	},
	{
		'name': 'О нас',
		'image': 'https://images.wallpaperscraft.ru/image/everest_gory_nebo_vershiny_96976_1920x1080.jpg',
		'content': '<h1 style="color: #fff; font-family: Ubuntu; font-weight: 100;">О нас и нашей компании</h1>'
	},
	{
		'name': 'Контакты',
		'image': 'http://wallpapers-image.ru/1920x1080/mountains/wallpapers/mountains-wallpapers-1920x1080-0000.jpg',
		'content': '<h1 style="color: #fff; font-family: Ubuntu; font-weight: 100;">Наши контакты</h1>'
	}
];